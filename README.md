# CovNCPlot

## Short Description

CovNCPlot - Plots COVID-19 new cases for a list of countries. Downloads the data
from [Our World in Data](https://covid.ourworldindata.org/) and uses Savitzky-Golay filter for smoothing. 

Requires: 

* numpy, 
* scipy, 
* urllib,
* matplotlib

## Screenshot

![screenshot](doc/screenshot.png)

## Usage

### Invocation
`covncplot.py [-h] [-c COUNTRIES] [-f]`

### Command Line Options
* -h, --help: show this help message and exit
* -c COUNTRIES, --countries COUNTRIES: Comma-separated list of countires. Partial names ar accepted as well. Example: 'Germany,Austria,Italy,Czech'
* -f, --figures: Plot to separate figures.

## Licesne

CovNCPlot - Plots COVID-19 new cases for a list of countries
Copyright © 2020 Dr. Petr Cizmar

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
