#!/usr/bin/env python3

'''
Licesne:

CovNCPlot - Plots COVID-19 new cases for a list of countries
Copyright © 2020 Dr. Petr Cizmar

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import os
import argparse
import math
import urllib.request as rq
import datetime
import re
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import savgol_filter
from scipy import interpolate

config = {
        "cntrys": ["Germany", "Czech", "Slovakia", "Austria", "Poland", "Hungary"],
        "perline": 2,
        "figures": False 
        }

def grab_data_owid():
    with rq.urlopen("https://covid.ourworldindata.org/data/ecdc/full_data.csv") as fp:
        linesbytes = fp.readlines()
    return linesbytes

def new_cases_country(owid, cntry, d={}):
    
    cre = re.compile(f"^[^,]*,{cntry}.*")

    dlines = [lnb.decode("utf8").rstrip() for lnb in owid]
    clists = [dl.split(",") for dl in dlines if cre.match(dl)]
    _cntry = clists[0][1]
    _d = [datetime.datetime.strptime(cl[0],'%Y-%m-%d') for cl in clists if cl[2]]
    _nc = [int(cl[2]) for cl in clists if cl[2]]

    nd = {"d": _d, "nc": _nc, "cntry": _cntry}
    d.update(nd)
    return d

def smooth_data(d, N):
    _nc = d.get("nc") # new cases
    _ts = [x.timestamp() for x in d.get("d")] # time as timestamp
    
    # linear interpolate missing days if any
    flinear = interpolate.interp1d(_ts, _nc)
    _tsint = np.arange(min(_ts), max(_ts), 1440)
    #_tsint += [max(_ts)]
    _ncint = flinear(_tsint)

    # Savitzky-Golay Filter
    _ncsmp = savgol_filter(_ncint, N*30+1, 2)
    _ncsm = savgol_filter(_ncsmp, N*30+1, 1)

    nd = {"ncint": _ncint, "ncsm": _ncsm, "tsint": _tsint}
    d.update(nd)

    
def plot_data(d):
    _cntry = d.get("cntry")
    _d = d.get("d")
    _nc = d.get("nc")

    _tsint = d.get("tsint")
    _dint = [datetime.datetime.fromtimestamp(t) for t in _tsint]
    _ncsm = d.get("ncsm")

    plt.plot(_d, _nc, "x", label=f"New Cases ({_cntry})")
    plt.plot(_d[-1], _nc[-1], marker="o", c="red", markersize=10, fillstyle="none",
            markeredgewidth=2, linestyle="", label=f"Last: {_nc[-1]}")
    plt.plot(_dint, _ncsm, "-", c="orange", label=f"New Cases Smoothed (Last: {int(_ncsm[-1])})")
    plt.yscale('log')
    plt.xlabel('Date')
    plt.xticks(rotation='vertical')
    plt.ylabel('Cases')
    plt.grid(True)
    plt.legend()
    
    axes = plt.gca()
    axes.set_ylim([1,max(_nc)*1.1])

def plot_country(owid, cntry, config):
    d = new_cases_country(owid, cntry)
    smooth_data(d, 30)

    _figures = config.get("figures")
    _perline = config.get("perline")
    _cntrys = config.get("cntrys")

    if config.get("figures"):
        plt.figure()
    else:
        _rows = math.ceil(len(_cntrys)/_perline)
        i = _cntrys.index(cntry)+1
        if i > 1:
            ax = plt.subplot(_rows, _perline, 1)
            plt.subplot(_rows, _perline, i, sharex=ax)
        else:
            plt.subplot(_rows, _perline, i)
    plot_data(d)

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--countries", help="Comma-separated list of countries (partial OK). Ex: 'Germany,Austria,Italy,Czech'")
    parser.add_argument("-f", "--figures", help="Plot to separate figures.", action="store_true")
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_arguments()
    if args.countries:
        config["cntrys"] = [c.strip() for c in args.countries.split(",")]
    if args.figures:
        config["figures"] = True

    if len(config.get("cntrys")) < 2: config["perline"] = 1
    owid = grab_data_owid()
    if not config.get("figures"):
        plt.title("COVID-19 New Cases Daily")
    for cntry in config.get("cntrys"):
        plot_country(owid, cntry, config)
    plt.subplots_adjust(hspace=0.05)
    plt.show(block=True)

